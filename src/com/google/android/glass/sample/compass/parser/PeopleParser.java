package com.google.android.glass.sample.compass.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.glass.sample.compass.enums.EDataBase;
import com.google.android.glass.sample.compass.model.StalkedPerson;

public class PeopleParser 
{
	private final String PATH = "res\\raw\\people.json";
	private List<StalkedPerson> m_victims = new ArrayList<StalkedPerson>();
	
	/**
	 * If you work without the web as your data base set the kaka mode to true.
	 * 
	 * @param isInkakaMode - if the web is not our DB
	 */
    public PeopleParser(boolean isInkakaMode) 
    {
    	if(isInkakaMode)
    	{
	    	File file = new File(PATH);
	
	    	if(file.exists())
	    	{
	    		try 
	    		{
					StringBuffer buffer = new StringBuffer();
					
					InputStream is = new FileInputStream(file);
					BufferedReader reader = new BufferedReader(new InputStreamReader(is));
					
					String line;
					try 
					{
						while ((line = reader.readLine()) != null) 
						{
						    buffer.append(line);
						    buffer.append('\n');
						}
						
						m_victims = giveMeVictims(buffer.toString());
					} 
					catch (Exception e)
					{
						e.printStackTrace();
					}
					finally
					{
						if (is != null) 
						{
							try 
							{
			                    is.close();
			                }
							catch (IOException e) 
			                {
								e.printStackTrace();
			                   //TODO: log
			                }
			            }
					}
				} 
	    		catch (FileNotFoundException e) 
				{
					// Shouldn't happen
					e.printStackTrace();
				}
	    	}
	    	else
	    	{
	    		System.out.println("The file in the path " + PATH + " doesn't exist");
	    		// TODO: log
	    	}
    	}
    	
    }
    
    /**
     * @return an empty list if the parser was initialized in kaka mode
     */
    public List<StalkedPerson> getFinalVictims()
    {
    	return m_victims;
    }
    
	public static List<StalkedPerson> giveMeVictims(String jsonVictims)
	{
		List<StalkedPerson> victims = new ArrayList<StalkedPerson>();
		
		try
		{
			JSONObject json = new JSONObject(jsonVictims);
	        JSONArray jsonVictimArray = json.optJSONArray("people");
	        
	        if (jsonVictimArray != null) 
	        {
                for (int i = 0; i < jsonVictimArray.length(); i++) 
                {
                    JSONObject object = jsonVictimArray.optJSONObject(i);
                    StalkedPerson victim = jsonObjectToVictim(object);
                  
                    if (victim != null)
                    {
                    	victims.add(victim);
                    }
                }
            }
	        
		}
		catch (Throwable e)
		{
			System.out.println("Could not parse people JSON string");
			e.printStackTrace();
		}
		
		return victims;
	}

	private static StalkedPerson jsonObjectToVictim(JSONObject object)
	{
		StalkedPerson retVal = null;
		
		String firstName = object.optString("first name");
		String lastName = object.optString("last name");
		EDataBase db = EDataBase.getDBByName(object.optString("db type"));
		double latitude = object.optDouble("latitude", Double.NaN);
	    double longitude = object.optDouble("longitude", Double.NaN);
	    String status = object.optString("status");
	    
	    if (!firstName.isEmpty() && !lastName.isEmpty() && !db.equals(EDataBase.UNDEFINED) &&
	    	!Double.isNaN(latitude) && !Double.isNaN(longitude) && !status.isEmpty())
	    {
	    	retVal = new StalkedPerson(latitude, longitude, firstName, lastName, db, status);
	    }
	    else
	    {
	    	System.out.println("Tried to create a person with the following data:");
	    	System.out.println("first name = " + firstName);
	    	System.out.println("last name = " + lastName);
	    	System.out.println("db = " + object.optString("db type"));
	    	System.out.println("latitude " + longitude);
	    	System.out.println("latitude = " + latitude);
	    	System.out.println("status = " + status);
	    }
	    	
	    
		return retVal;
	}
}
