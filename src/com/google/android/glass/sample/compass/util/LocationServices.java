package com.google.android.glass.sample.compass.util;

import com.google.android.glass.sample.compass.model.Place;

public interface LocationServices {
	public float getDistnaceInMeters(Place myPlace, Place other);
	
	public float getDistanceInSteps(float distanceInMeter);
}
