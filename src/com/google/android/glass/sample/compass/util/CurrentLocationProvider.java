package com.google.android.glass.sample.compass.util;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.glass.sample.compass.model.Place;

public class CurrentLocationProvider implements CompassLocationProvider {
	private static final int MIN_SAMPLING_DISTANCE_INTERVAL = 50;
	private static final int MIN_SAMPLING_TIME_INTERVAL = 2000;
	private static final String CURRENT_LOCATION_STRING = "CurrentLocation";
	private LocationManager locationManager;
	private Place currentLocation;
	
	public CurrentLocationProvider(LocationManager locationManager) {
		this.locationManager = locationManager;
		
		// In this example we ask for fine accuracy and require altitude, but
		// these criteria could be whatever you want.
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setAltitudeRequired(false);

		String providerName = locationManager.getBestProvider(criteria, true);
		
		locationManager.requestLocationUpdates(providerName, MIN_SAMPLING_TIME_INTERVAL,
				                               MIN_SAMPLING_DISTANCE_INTERVAL, new GlassLocationListener());
	}
	
	@Override
	public Place getLocation() {
		return currentLocation;
	}
	
	private class GlassLocationListener implements LocationListener {
		@Override
		public void onLocationChanged(Location location) {
			currentLocation = new Place(location.getLatitude(), location.getLongitude(), CURRENT_LOCATION_STRING);
		}

		@Override
		public void onProviderDisabled(String provider) {
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			
		}
	}
}
