package com.google.android.glass.sample.compass.util;

import com.google.android.glass.sample.compass.model.Place;

public class HubSelfLocationProvider implements CompassLocationProvider {
	private static final float HUB_LAT = 32.067933333333333f;
	private static final float HUB_LON = 34.78588333333333f;
	//32.067933333333333 ,34.78588333333333
	
	@Override
	public Place getLocation() {
		return new Place(HUB_LAT, HUB_LON, "Hub Tel-Aviv");
	}

}
