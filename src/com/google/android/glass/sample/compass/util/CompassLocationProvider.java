package com.google.android.glass.sample.compass.util;

import com.google.android.glass.sample.compass.model.Place;

public interface CompassLocationProvider {
	public Place getLocation();
}
