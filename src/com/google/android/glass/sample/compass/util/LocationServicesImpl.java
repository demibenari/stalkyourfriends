package com.google.android.glass.sample.compass.util;

import com.google.android.glass.sample.compass.model.Place;

public class LocationServicesImpl implements LocationServices {
	@Override
	public float getDistnaceInMeters(Place myPlace, Place other) {		
		return MathUtils.getDistance(myPlace.getLatitude(), myPlace.getLongitude(), 
								     other.getLatitude(), other.getLongitude());
	}

	@Override
	public float getDistanceInSteps(float distanceInMeter) {
		return distanceInMeter / 1300;
	}
}
