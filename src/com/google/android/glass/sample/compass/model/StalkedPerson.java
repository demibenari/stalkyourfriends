package com.google.android.glass.sample.compass.model;

import com.google.android.glass.sample.compass.enums.EDataBase;

public class StalkedPerson
{
	private final double m_latitude;
	private final double m_longitude;
	private final String m_firstName;
	private final String m_lastName;
	private final EDataBase m_db;
	private final String m_status;
	
    public StalkedPerson(double latitude, double longitude, String firstName, String lastName, EDataBase db,
    		String status) 
    {
        m_latitude = latitude;
        m_longitude = longitude;
        m_firstName = firstName;
        m_lastName = lastName;
        m_db = db;
        m_status = status;
    }
    
    public String getFirstName()
    {
    	return m_firstName;
    }

	public double geLongitude()
	{
		return m_longitude;
	}

	public double getLatitude()
	{
		return m_latitude;
	}

	public String getLastName() 
	{
		return m_lastName;
	}

	public EDataBase getDB()
	{
		return m_db;
	}
	
	@Override
	public String toString() 
	{
		return m_firstName + " " + m_lastName + " " + m_db;
	}

	public String getStatus()
	{
		return m_status;
	}
}