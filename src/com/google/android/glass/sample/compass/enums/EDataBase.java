package com.google.android.glass.sample.compass.enums;

import java.util.HashMap;
import java.util.Map;

public enum EDataBase
{
	FACEBOOK("facebook"),
	GOOGLE_PLUS("google+"),
	LINKED_IN("linked in"),
	UNDEFINED("");
	
	private static Map<String, EDataBase> m_dbMap;
	
	static
	{
		m_dbMap = new HashMap<String, EDataBase>();
		
		for (EDataBase currDB : values()) 
		{
			m_dbMap.put(currDB.getDBName(), currDB);
		}
	}
	
	public static EDataBase getDBByName(String name)
	{
		EDataBase retVal;
		
		if(name != null)
		{
			retVal = m_dbMap.get(name);
			
			if(retVal == null)
			{
				retVal = UNDEFINED;
			}
		}
		else
		{
			retVal = UNDEFINED;
		}
		
		return retVal;
	}
	
	private String m_dbName;
	
	private EDataBase(String dbName) 
	{
		m_dbName = dbName;
	}
	
	private String getDBName()
	{
		return m_dbName;
	}
}
