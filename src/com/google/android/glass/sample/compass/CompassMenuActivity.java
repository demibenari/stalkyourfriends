/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.glass.sample.compass;

import java.io.IOException;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

/**
 * This activity manages the options menu that appears when the user taps on the
 * compass's live card.
 */
public class CompassMenuActivity extends Activity {

	private CompassService.CompassBinder mCompassService;
	private boolean mResumed;
	private CompassService mCompassServiceNB;
	private GestureDetector mGestureDetector;
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			if (service instanceof CompassService.CompassBinder) {
				mCompassService = (CompassService.CompassBinder) service;

				openOptionsMenu();
			}

		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// Do nothing.
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("Activity", "Started this");
		System.out.println("activity Started");
		mGestureDetector = createGestureDetector(this);

		bindService(new Intent(this, CompassService.class), mConnection, 0);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mResumed = true;
		openOptionsMenu();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mResumed = false;
	}

	@Override
	public void openOptionsMenu() {
		if (mResumed && mCompassService != null) {
			super.openOptionsMenu();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(1, 1, 1, "Omri").setIcon(R.drawable.ic_omri);
		menu.add(1, 2, 2, "Demi").setIcon(R.drawable.ic_demi);
		menu.add(1, 3, 3, "Yuri").setIcon(R.drawable.ic_yuri);
		menu.add(1, 4, 4, "Michael").setIcon(R.drawable.ic_demi);
		menu.add(1, 5, 5, "Meir").setIcon(R.drawable.ic_meir);
		menu.add(1, 6, 6, "Exit").setIcon(R.drawable.ic_stop);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		playSample(R.raw.sound_1);
		switch (item.getItemId()) {
		case 1:
			mCompassService.setStalker(1);
			return true;
		case 2:
			mCompassService.setStalker(2);
			return true;
		case 3:
			mCompassService.setStalker(3);
			return true;
		case 4:
			mCompassService.setStalker(4);
			return true;
		case 5:
			mCompassService.setStalker(5);
			return true;
		case 6:
			stopService(new Intent(this, CompassService.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void playSample(int resid) {
		MediaPlayer mediaPlayer = new MediaPlayer();
		AssetFileDescriptor afd = this.getResources().openRawResourceFd(resid);

		try {
			mediaPlayer.reset();
			mediaPlayer.setDataSource(afd.getFileDescriptor(),
					afd.getStartOffset(), afd.getDeclaredLength());
			mediaPlayer.prepare();
			mediaPlayer.start();
			afd.close();
		} catch (IllegalArgumentException e) {

		} catch (IllegalStateException e) {

		} catch (IOException e) {

		}
	}

	@Override
	public void onOptionsMenuClosed(Menu menu) {
		super.onOptionsMenuClosed(menu);

		unbindService(mConnection);

		// We must call finish() from this method to ensure that the activity
		// ends either when an
		// item is selected from the menu or when the menu is dismissed by
		// swiping down.
		finish();
	}

	private GestureDetector createGestureDetector(Context context) {
		GestureDetector gestureDetector = new GestureDetector(context);
		// Create a base listener for generic gestures
		gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
			@Override
			public boolean onGesture(Gesture gesture) {
				if (gesture == Gesture.TAP) {
					Log.d("Gesture Detection", "TAP");
					return true;
				} else if (gesture == Gesture.TWO_TAP) {
					Log.d("Gesture Detection", "TWO_TAP");
					return true;
				} else if (gesture == Gesture.SWIPE_RIGHT) {
					Log.d("Gesture Detection", "SWIPE_RIGHT");
					return true;
				} else if (gesture == Gesture.SWIPE_LEFT) {
					Log.d("Gesture Detection", "SWIPE_LEFT");
					return true;
				}
				return false;
			}
		});
		gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
			@Override
			public void onFingerCountChanged(int previousCount, int currentCount) {
				// do something on finger count changes
			}
		});

		return gestureDetector;
	}

	/*
	 * Send generic motion events to the gesture detector
	 */
	@Override
	public boolean onGenericMotionEvent(MotionEvent event) {
		if (mGestureDetector != null) {
			return mGestureDetector.onMotionEvent(event);
		}
		return false;
	}

}
